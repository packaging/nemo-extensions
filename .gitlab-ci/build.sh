#!/bin/bash

apt-get update
apt-get -y install git debhelper
git clone https://github.com/linuxmint/xreader $CI_PROJECT_DIR/xreader
git clone https://github.com/linuxmint/nemo-extensions $CI_PROJECT_DIR/nemo-extensions
cd $CI_PROJECT_DIR/xreader
dpkg-buildpackage -us -uc 2>&1 | tee /tmp/xreader.build-deps
grep -o 'Unmet build dependencies.*' /tmp/xreader.build-deps | sed 's, ([<>=]*\s[0-9\.]*),,g' | cut -d':' -f2 | xargs apt-get -y install
dpkg-buildpackage -us -uc
dpkg -i $CI_PROJECT_DIR/*.deb
cd $CI_PROJECT_DIR/nemo-extensions
bash buildall 2>&1 | tee /tmp/nemo-extensions.build-deps
grep -o 'Unmet build dependencies.*' /tmp/nemo-extensions.build-deps | sed 's, ([<>=]*\s[0-9\.]*),,g' | cut -d':' -f2 | xargs apt-get -y install
bash buildall
mkdir $CI_PROJECT_DIR/$CI_JOB_NAME
mv $CI_PROJECT_DIR/*.deb $CI_PROJECT_DIR/nemo-extensions/*.deb $CI_PROJECT_DIR/$CI_JOB_NAME/
