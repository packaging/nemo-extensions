# nemo-extensions

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-nemo-extensions.asc https://packaging.gitlab.io/nemo-extensions/gpg.key
```

### Add repo to apt

```bash
. /etc/lsb-release; echo "deb [arch=amd64] https://packaging.gitlab.io/nemo-extensions $DISTRIB_CODENAME main" | sudo tee /etc/apt/sources.list.d/morph027-nemo-extensions.list
```
